﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Article
    {
        public string product_name { get; }
        public string shop_name { get; }
        public double price { get; }

        public Article(string product_name, string shop_name, double price)
        {
            this.product_name = product_name;
            this.shop_name = shop_name;
            this.price = price;
        }
    }
}
