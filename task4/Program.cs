﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Store store = new Store();
            int size = store.GetSize();
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(store[i].product_name + " " + store[i].shop_name + " " + store[i].price);
            }

            Console.WriteLine();
            for (int i = 0; i < size + 1; i++)
            {
                Console.WriteLine(store["Good"+Convert.ToString(i)]);
            }
        }
    }
}