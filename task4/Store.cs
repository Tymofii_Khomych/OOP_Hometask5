﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Store
    {
        Article[] articles =
        {
            new Article("Good1", "Shop1", 10),
            new Article("Good2", "Shop2", 20),
            new Article("Good3", "Shop3", 30),
            new Article("Good4", "Shop4", 40),
            new Article("Good5", "Shop5", 50),
        };

        public int GetSize()
        {
            return articles.Length;
        }
        public Article this[int index]
        {
            get
            {
                if(index >= 0 && index < articles.Length)
                {
                    return articles[index];
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        public string this[string name]
        {
            get
            {
                foreach (Article article in articles)
                {
                    if (article.product_name == name)
                    {
                        return $"Product name: {article.product_name}\nShop name: {article.shop_name}\nPrice: {article.price}\n";
                    }
                }
                return "Article not found\n";
            }
        }
    }
}
