﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input number of rows: ");
            int row = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input number of columns: ");
            int col = Convert.ToInt32(Console.ReadLine());

            MyMatrix matrix = new MyMatrix(row, col);
            Console.WriteLine("\nOriginal matrix");
            matrix.Print();

            while (row > 0 && col > 0)
            {
                Console.Write("\nInput row to erase: ");
                int row_toerase = Convert.ToInt32(Console.ReadLine());
                Console.Write("Input column to erase: ");
                int col_toerase = Convert.ToInt32(Console.ReadLine());
                if (row_toerase >= 0 && col_toerase >= 0 && row_toerase < matrix.row && col_toerase < matrix.col)
                {
                    matrix.OrderReduction(row_toerase, col_toerase);
                    matrix.Print();
                }
                else
                {
                    Console.WriteLine("Wrong input");
                }
            }
        }
    }
}