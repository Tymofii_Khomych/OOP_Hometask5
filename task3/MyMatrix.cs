﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class MyMatrix
    {
        public int row { get; set; }
        public int col { get; set; }
        int[,] matrix;
        public MyMatrix(int row, int col)
        {
            this.row = row;
            this.col = col;

            matrix = new int[row, col];

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    Random rand = new Random();
                    matrix[i, j] = rand.Next(-100, 100);
                }
            }
        }

        public void Print()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

        public void OrderReduction(int row_erase, int col_erase)        // пониження порядку
        {
            int new_row = row - 1;
            int new_col = col - 1;
            int[,] new_matrix = new int[new_row, new_col];

            int n = 0;
            for (int i = 0; i < row; i++)
            {
                int m = 0;
                if (i != row_erase)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (j != col_erase)
                        {
                            new_matrix[n, m] = matrix[i, j];
                            m++;
                        }
                    }
                    n++;
                }
            }
            row -= 1;
            col -= 1;
            

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matrix[i, j] = new_matrix[i, j];

                }
            }
        }

    }
}
