﻿namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input size of array:");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[size];

            Console.WriteLine("\nOriginal array:");
            for (int i = 0; i < size; i++)
            {
                Random random = new Random();
                array[i] = random.Next(-100, 100);
                Console.Write(array[i] + " ");
            }

            Console.WriteLine($"\n\nMax number in array = {array.Max()}");
            Console.WriteLine($"Min number in array = {array.Min()}");
            Console.WriteLine($"Sum of array = {array.Sum()}");
            Console.WriteLine($"Average of array = {(double)array.Sum() / size}");

            Console.WriteLine("\nOdd numebers in array:");
            foreach(int i in array)
            {
                if(i % 2 != 0)
                {
                    Console.Write(i + " ");
                }
            }
            Console.WriteLine();
        }
    }
}