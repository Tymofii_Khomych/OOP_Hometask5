﻿using System;
using System.Text;

// Індексатори.

namespace Indexers
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Dictionary dictionary = new Dictionary();

            Console.WriteLine(dictionary["книга"]);
            Console.WriteLine(dictionary["дім"]);
            Console.WriteLine(dictionary["pen"]);
            Console.WriteLine(dictionary["стол"]);
            Console.WriteLine(dictionary["олівець"]);
            Console.WriteLine(dictionary["яблоко"]);
            Console.WriteLine(dictionary["солнце"]);

            Console.WriteLine(new string('-', 20));

            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine(dictionary[i]);
            }
                        
            // Delay.
            Console.ReadKey();
        }
    }
}
