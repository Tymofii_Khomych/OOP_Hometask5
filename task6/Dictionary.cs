﻿using System;

namespace Indexers
{
    class Dictionary
    {
        private string[] ukrainian = new string[5];
        private string[] english = new string[5];
        private string[] russian = new string[5];

        public Dictionary()
        {
            ukrainian[0] = "книга";     english[0] = "book";    russian[0] = "книга";
            ukrainian[1] = "ручка";     english[1] = "pen";     russian[1] = "ручка";
            ukrainian[2] = "сонце";     english[2] = "sun";     russian[2] = "солнце";
            ukrainian[3] = "яблуко";    english[3] = "apple";   russian[3] = "яблоко";
            ukrainian[4] = "стіл";      english[4] = "table";   russian[4] = "стол";
        }

        public string this[string index]
        {
            get
            {
                for (int i = 0; i < ukrainian.Length; i++)
                    if (ukrainian[i] == index || english[i] == index || russian[i] == index)
                        return ukrainian[i] + " - " + english[i] + " - " + russian[i];

                return string.Format("{0} - немає перекладу для цього слова.", index);
            }
        }

        public string this[int index]
        {
            get
            {
                if (index >= 0 && index < ukrainian.Length)
                    return ukrainian[index] + " - " + english[index] + " - " + russian[index];
                else
                    return "Спроба звернення за межі масиву.";
            }
        }      
    }
}
